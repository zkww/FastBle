package com.clj.blesample.notication;

import android.bluetooth.BluetoothDevice;

import java.util.ArrayList;

public interface ScanResultCallback {
    void onScanResult(ArrayList<BluetoothDevice> devices);
}
