package com.clj.blesample.notication;

import android.util.Log;

public class WatchBleLog {
    private static boolean sEnable = true;
    private static final String TAG = "[LogTool]";

    public static void logV(String content) {
        if (!sEnable) {
            return;
        }
        Log.v(TAG, content);
    }

    public static void logD(String content) {
        if (!sEnable) {
            return;
        }
        Log.d(TAG, content);
    }

    public static void logI(String content) {
        if (!sEnable) {
            return;
        }
        Log.i(TAG, content);
    }

    public static void logW(String content) {
        if (!sEnable) {
            return;
        }
        Log.w(TAG, content);
    }

    public static void logE(String content) {
        if (!sEnable) {
            return;
        }
        Log.e(TAG, content);
    }

    public static void setEnable(boolean enable) {
        sEnable = enable;
    }
}
