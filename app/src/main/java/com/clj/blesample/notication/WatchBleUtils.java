package com.clj.blesample.notication;

import android.annotation.SuppressLint;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.clj.blesample.R;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.content.pm.PackageManager.GET_ACTIVITIES;

public class WatchBleUtils {

    public static boolean isWatchApp(String pkg) {
        if (isSms(pkg)) {
            return true;
        }
        if (isPhone(pkg)) {
            return true;
        }
        String[] pkgNames = new String[]{"com.tencent.mm", "com.android.dialer",
                "com.android.mms", "com.whatsapp",};
        for (String pkgName : pkgNames) {
            if (pkg.equals(pkgName)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isSms(String pkg) {
        if (pkg.startsWith("com") && pkg.endsWith("mms")) {
            return true;
        }
        return false;
    }

    private static boolean isPhone(String pkg) {
        pkg = pkg.toLowerCase();
        if ((pkg.startsWith("com") && pkg.endsWith("dialer"))) {
            return true;
        }
        return false;
    }

    /**
     * 格式化内容，部分应用存在数据格式不对，需要进行优化
     *
     * @param context
     * @param pkg
     * @param title
     * @param content
     * @return
     */
    public static String convertContent(Context context, String pkg, String title, String content) {
        if (pkg.equals("com.tencent.mm")) {//微信内容优化
            int subIndex = content.indexOf("]");//截取消息[5条]Zzzzzzz: 请问
            if (subIndex != -1) {
                content = content.substring(subIndex + 1);
            }
            return content;
        } else if (pkg.equals("com.mms")) {//用户手机短信
            if (title.contains("条新信息") || title.contains("new messages")) {//中英文通知
                if (content.indexOf(",") != -1) {
                    content = content.substring(0, content.indexOf(","));
                } else if (content.indexOf("、") != -1) {
                    content = content.substring(0, content.indexOf("、"));
                }
                return context.getString(R.string.new_message_form) + content;
            } else {//单条新未读消息
                return context.getString(R.string.new_message_form) + content;
            }
        }
        return content;
    }

    /**
     * 优化应用不需要的title
     *
     * @param context
     * @param pkg
     * @param title
     * @param content
     * @return
     */
    public static String getNoticationTitle(Context context, String pkg, String title, String content) {
        //1.把微信的title取消，保留内容发送即可
        if (pkg.equals("com.tencent.mm")) {
            if (title.indexOf("、") != -1
                    || title.indexOf(",") != -1
                    || title.equals(context.getString(R.string.group_chat))) {
                title = "";
            } else {
                title = "";
            }
        } else if (pkg.equals("com.whatsapp")) {//去掉whatsApp的title，因为和内容的数据重复了
            title = "";
        }
        return title;
    }

    /**
     * 过滤监听通知的应用包名
     *
     * @param pkg
     * @return
     */
    public static boolean isListentNoticationPkg(String pkg) {
        if (pkg.equals("com.whatsapp")//马来聊天
                || pkg.equals("com.tencent.mm")//微信
                || pkg.equals("com.android.mms") //短信应用
                || pkg.equals("com.mms") //短信应用
                || pkg.equals("com.android.mms.service") //短信应用
                || pkg.equals("com.android.incallui")//电话
                || pkg.equals("com.dialer")//电话
                || pkg.equals("com.android.contacts")//电话
                || pkg.equals("com.android.phone")//电话
                || pkg.equals("com.android.server.telecom")//电话

        ) {
            return true;
        }
        return false;
    }

}
