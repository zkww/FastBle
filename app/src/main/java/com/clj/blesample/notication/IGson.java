package com.clj.blesample.notication;

public interface IGson {
    String convertObjectToJson(Object o);
}
